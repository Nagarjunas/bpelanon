/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class XSD_Scanner {

	TreeSet<String> olddatanames = new TreeSet<String>();
	LinkedList<String> newdatanames = new LinkedList<String>();
	
/*
 * used for scanning the XSD file
 * @param File name of the XSD file
 * @param File_bpel name of the BPEL file
 * @param file_wsdl_edited list of the names of the WSDL file
 * @param file_xsd_edited list of XSD file names
 * @param oldCDATA to store old CDATA values
 * @param newCDATA to sore new CDATA values
 */
	public void xsdScanner(String File,String File_bpel,List<String> file_wsdl_edited,List<String>file_xsd_edited,
			List<String> oldCDATA , List<String> newCDATA) throws GeneralException {
		olddatanames.clear();
		newdatanames.clear();
		
		String randomstring = null;
		String namespacexsd = null;
		String namespaceprefix = null;
		
		List<String> namespace = new LinkedList<String>();
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		
		Document document = utils_obj.documentBuilder(File);
		
		if(document.hasChildNodes()){
			namespacexsd = utils_obj.getTargetNameSpaceXSD(document.getChildNodes());	//gets target name space of xsd file
			utils_obj.findNameSpacePrefix(document.getChildNodes(), namespacexsd, namespace);  //finds the name space prefix of xsd file
			
			if(namespace.size()!=0){
				namespaceprefix=namespace.get(0);
			}
			
			finddatavaribles_xsd(document.getChildNodes(), olddatanames);

			for (String olddataname : olddatanames) {
				ReNamer renamer_obj = new ReNamer();
				randomstring = renamer_obj.anonymizer();
				newdatanames.add(randomstring);
				updatedatavaribles_xsd(document.getChildNodes(), olddataname, randomstring,namespaceprefix);
				renamer_object.comitChanges(document, File);
			}
		}
		
		updatebpelxsd(File_bpel, namespacexsd);
		
		oldCDATA.addAll(olddatanames);
		newCDATA.addAll(newdatanames);

	}

	/*
	 * updates the xsd files based on the changes from other xsd files
	 * @param file_wsdl name of the WSDL file
	 * @param file_xsd name of the XSD file
	 */
	public void xsd_wsdlupdater(String file_wsdl,String file_xsd) {
		
		int j=0;
		String namespacexsd = null;
		
		List<String> namespace = new LinkedList<String>();
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		Registry registry_obj = new Registry();
		
		Document document_wsdl = utils_obj.documentBuilder(file_wsdl);
		Document document_xsd = utils_obj.documentBuilder(file_xsd);
		
		if(document_xsd.hasChildNodes()){
			namespacexsd = utils_obj.getTargetNameSpaceXSD(document_xsd.getChildNodes());
		}

		if(document_wsdl.hasChildNodes()){
			utils_obj.findNameSpacePrefix(document_wsdl.getChildNodes(), namespacexsd, namespace);
			if(namespace.size()!=0){
				j=0;
				for (String olddataname : olddatanames) {
					String modified_to = newdatanames.get(j++);
					registry_obj.updatewsdl_xsd(document_wsdl.getChildNodes(), olddataname, modified_to, namespace.get(0));
					renamer_object.comitChanges(document_wsdl, file_wsdl);
				}
			}
		}
	}



	/*
	 * finds the schemas names of the data variables and enumeration
	 * @param nodeList list of nodes of the XSD file
	 * @param olddatanames old names of data varibles
	 */
	public void finddatavaribles_xsd(NodeList nodeList,TreeSet<String> olddatanames) throws GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeName().compareTo("name")==0) { 
							olddatanames.add(currentAttribute.getNodeValue());
						}
						if(tempNode.getNodeName().contains("enumeration")){
							if(currentAttribute.getNodeName().equals("value")){
								olddatanames.add(currentAttribute.getNodeValue());
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					finddatavaribles_xsd(tempNode.getChildNodes(),olddatanames);
				}
			}
		}
	}

	/*
	 * anonymizes the data variable and enumeration
	 * @param nodeList list of the XSD nodes
	 * @param olddataname name of old data
	 * @param newdataname name which replaces the old data name
	 * @param namespaceprefix namespace prefix 
	 */

	public void updatedatavaribles_xsd(NodeList nodeList,String olddataname, String newdataname, String namesaceprefix) throws GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeName().compareTo("name")==0 && (currentAttribute.getNodeValue().equals(olddataname))) { 
							currentAttribute.setNodeValue(newdataname);
						}
						if(currentAttribute.getNodeValue().equals(namesaceprefix+olddataname)){
							currentAttribute.setNodeValue(namesaceprefix+newdataname);
						}
						if(tempNode.getNodeName().contains("enumeration")){
							if(currentAttribute.getNodeName().equals("value") && (currentAttribute.getNodeValue().equals(olddataname)) ){
								currentAttribute.setNodeValue(newdataname);
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					updatedatavaribles_xsd(tempNode.getChildNodes(),olddataname,newdataname,namesaceprefix);
				}
			}
		}
	}
	
	
	/*
	 * upadate the variable initialization in bpel file based on xsd file
	 * 
	 */
	
	public void updatebpelxsd(String bpel_file, String namespace) {
		
		String tns = "";
		String to_be_modified = "";
		int j = 0;
		int j1 = 0;
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		
		Document document = utils_obj.documentBuilder(bpel_file);
		
		if(document.hasChildNodes()){
			NodeList bpel_literals = document.getElementsByTagName("bpel:literal");
			for (int i = 0; i < bpel_literals.getLength(); i++) {
				Node tempNode = bpel_literals.item(i);
				if(tempNode.hasChildNodes()){
					NodeList child_nodes = tempNode.getChildNodes();
					for (int i1 = 0; i1 < child_nodes.getLength(); i1++) {
						Node child_node = child_nodes.item(i1);
						if(child_node.hasAttributes()){
							NamedNodeMap nodeMap = child_node.getAttributes();
							for (int i2 = 0; i2 < nodeMap.getLength(); i2++) {
								Node currentAttribute = nodeMap.item(i2);
								if(currentAttribute.getNodeName().startsWith("xmlns:") && currentAttribute.getNodeValue().equals(namespace)){
									tns = child_node.getNodeName().split(":")[0];
									if(currentAttribute.getNodeName().split(":")[1].equals(tns)){
										to_be_modified = child_node.getNodeName().split(":")[1];
										j=0;
										for (String olddataname : olddatanames) {
											String modified_to = newdatanames.get(j++);
											if(olddataname.equals(to_be_modified)){
												document.renameNode(child_node, namespace, modified_to);
												child_node.setPrefix(tns);
												if(child_node.hasChildNodes()){
													NodeList grandchilds = child_node.getChildNodes();
													for (int i3 = 0; i3 < grandchilds.getLength(); i3++) {
														Node grandchild = grandchilds.item(i3);
														to_be_modified = grandchild.getNodeName();
														j1 = 0;
														for (String olddataname1 : olddatanames) {
															modified_to = newdatanames.get(j1++);
															if(olddataname1.equals(to_be_modified)){
																document.renameNode(grandchild, null, modified_to);
																break;
															}
														}
														
													}
												}
												break;
											}
										}
										
									}
								}
							}
						}
					}
				}
			}
		}
		renamer_object.comitChanges(document, bpel_file);
	}
	
	

}




