/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import org.w3c.dom.Document;


public class Main_controller {

	public void mainFunction(String folder) throws GeneralException {

		String file_wsdl = null;
		String file_bpel = null;
		String file_xsd = null;
		String file_deploy = null;
		String file_deploy_edited=null;

		TreeSet<String> nodeNames = new TreeSet<String>();			// list out the node names to be modified
		List<String> oldAttributes = new LinkedList<String>();		// list the old attributes in Bpel file	
		List<String> newAttributes = new LinkedList<String>();		// list the modifications made to the bpel file attributes
		
		List<String> oldCDATA = new LinkedList<String>();			//list of old CDATA in XSD file
		List<String> newCDATA = new LinkedList<String>();			//list of new CDATA in XSD file
		
		List<String> oldBpelvar = new LinkedList<String>();			//list of old bpel varibles
		List<String> newBpelvar = new LinkedList<String>();			//list of new bpel varibles
	
		List<String> oldwsdlattributes = new LinkedList<String>();	// list the old attributes in Wsdl file	
		List<String> newwsdlattributes = new LinkedList<String>();	// list the new attributes in Wsdl file	
		
		Utilities util_obj = new Utilities();
		WSDL_Scanner wsdl_scaner_object = new WSDL_Scanner();
		BPEL_Scanner bpel_scaner_obj = new BPEL_Scanner();
		XSD_Scanner xsd_scanner_obj = new XSD_Scanner();
		ReNamer renamer_object = new ReNamer();

		File wsdl[] = util_obj.finder(folder,"wsdl");
		File bpel[] = util_obj.finder(folder,"bpel");
		File xsd[]  = util_obj.finder(folder,"xsd");
		File deploy[] = util_obj.finder(folder, "xml");
		
		/*
		 * Throws an error if a bpel file is not detected in the project folder
		 */
		if(bpel.length==0){
			throw new GeneralException("Select a valid BPEL Project folder");
		}

		Boolean success = (new File(folder+"/Export")).mkdirs();	 //create the Export Folder

		if (!success) {
			throw new GeneralException("Folder named Export already Exists!");
		}

		/*
		 *Create backup of different bpel, Wsdl and xsd files within the project directory 
		 */
		if(deploy.length!=0){
			file_deploy = deploy[0].getAbsolutePath();					// get path for deployment descriptor file
			System.out.println(deploy[0].getAbsolutePath());

			file_deploy_edited = file_deploy.substring(0,folder.length()) + "\\Export\\"+
					file_deploy.substring(folder.length()+1 ,file_deploy.indexOf("."))+
					file_deploy.substring(file_deploy.indexOf("."),file_deploy.length());
			
			util_obj.backupCreator(file_deploy,file_deploy_edited);				//backup the files
		}
		
		file_bpel = bpel[0].getAbsolutePath();					// get path for bpel file
		System.out.println(bpel[0].getAbsolutePath());

		String file_bpel_edited = file_bpel.substring(0,folder.length()) + "\\Export\\"+
				file_bpel.substring(folder.length()+1 ,file_bpel.indexOf("."))+"_edited"+
				file_bpel.substring(file_bpel.indexOf("."),file_bpel.length());

		util_obj.backupCreator(file_bpel,file_bpel_edited);				//backup the files

		List<String> file_xsd_edited = new LinkedList<String>();
		for (int i = 0; i < xsd.length; i++) {
			file_xsd = xsd[i].getAbsolutePath();
			System.out.println(xsd[i].getAbsolutePath());
			file_xsd_edited.add(file_xsd.substring(0,folder.length()) + "\\Export\\"+
					file_xsd.substring(folder.length()+1 ,file_xsd.indexOf("."))+"_edited"+
					file_xsd.substring(file_xsd.indexOf("."),file_xsd.length()));

			util_obj.backupCreator(file_xsd,file_xsd_edited.get(i));		//backup the files
		}

		List<String> file_wsdl_edited = new LinkedList<String>();

		for (int i = 0; i < wsdl.length; i++) {
			file_wsdl = wsdl[i].getAbsolutePath();
			System.out.println(wsdl[i].getAbsolutePath());
			file_wsdl_edited.add(file_wsdl.substring(0,folder.length()) + "\\Export\\"+
					file_wsdl.substring(folder.length()+1 ,file_wsdl.indexOf("."))+"_edited"+
					file_wsdl.substring(file_wsdl.indexOf("."),file_wsdl.length()));

			util_obj.backupCreator(file_wsdl,file_wsdl_edited.get(i));		//backup the files
		}

		
		
		for (int i = 0; i < wsdl.length; i++) {
			try {
				oldAttributes.clear();
				newAttributes.clear();
				util_obj.removeCommenting(file_wsdl_edited.get(i));					//Remove comments in the wsdl files
				wsdl_scaner_object.wsdlScanner(nodeNames,file_wsdl_edited.get(i));
				wsdl_scaner_object.findAttributes(nodeNames, file_wsdl_edited.get(i), oldAttributes, newAttributes);
				util_obj.removeCommenting(file_bpel_edited);						//Remove comments in the project files
				bpel_scaner_obj.editBpel(file_bpel_edited, file_wsdl_edited.get(i),oldAttributes,newAttributes);
				oldwsdlattributes.addAll(oldAttributes);
				newwsdlattributes.addAll(newAttributes);
				wsdl_scaner_object.editDeploy(file_deploy_edited,oldAttributes,newAttributes);

				for (int j = 0; j < wsdl.length; j++) {
					wsdl_scaner_object.wsdlCompararer(file_wsdl_edited.get(j), file_wsdl_edited.get(i),oldAttributes,newAttributes);
				}


			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

		}
		
		bpel_scaner_obj.freeElementGroup(file_bpel_edited,oldBpelvar,newBpelvar);
		bpel_scaner_obj.editdeploy(file_deploy_edited);

		for (int i = 0; i < xsd.length; i++) {
			xsd_scanner_obj.xsdScanner(file_xsd_edited.get(i),file_bpel_edited,file_wsdl_edited,file_xsd_edited,oldCDATA,newCDATA);
			for (int j = 0; j < xsd.length; j++) {
				if(j==i)
					continue;
				xsd_scanner_obj.xsd_wsdlupdater(file_xsd_edited.get(j),file_xsd_edited.get(i));
			}
			for (int j = 0; j < wsdl.length; j++) {
				xsd_scanner_obj.xsd_wsdlupdater(file_wsdl_edited.get(j),file_xsd_edited.get(i));
			}
		}
		
		editImports(file_bpel_edited);
		for (int i = 0; i < xsd.length; i++) {
			editImports(file_xsd_edited.get(i));
		}
		
		for (int i = 0; i < wsdl.length; i++) {
			editImports(file_wsdl_edited.get(i));
		} 
		
		
		/*
		 * Corelation
		 */
		for (int i = 0; i < wsdl.length; i++) {
			Document document = util_obj.documentBuilder(file_wsdl_edited.get(i));
		//	System.out.println(file_wsdl_edited.get(i));
			wsdl_scaner_object.updateCorelation(document.getChildNodes(), oldwsdlattributes, newwsdlattributes);
			renamer_object.comitChanges(document, file_wsdl_edited.get(i));
		}
		
		/*
		 * Correlation property
		 */
		for (int i =0; i < wsdl.length; i++) {
			wsdl_scaner_object.updateCorelationProp(file_wsdl_edited.get(i), file_bpel_edited);
		}
		
		

		/*
		 * CDATA 
		 */
		CDATA_Scanner cdata_obj = new CDATA_Scanner();
		Document document_bpel_cdata = util_obj.documentBuilder(file_bpel_edited);
		cdata_obj.nameSpacefinder(document_bpel_cdata.getChildNodes());
		cdata_obj.bpelvarCopier(oldBpelvar ,newBpelvar);
		cdata_obj.bpelVariableScanner(document_bpel_cdata.getChildNodes(),file_wsdl_edited, file_xsd_edited);
		cdata_obj.xsdAttributeMapper(file_bpel_edited,oldCDATA,newCDATA);
		for (int i = 0; i < wsdl.length; i++) {
			Document document = util_obj.documentBuilder(file_wsdl_edited.get(i));
		//	System.out.println(file_wsdl_edited.get(i));
			cdata_obj.updateCorelationCDATA(document.getChildNodes(), oldCDATA,newCDATA);
			renamer_object.comitChanges(document, file_wsdl_edited.get(i));
		}
		cdata_obj.anonString(file_bpel_edited);
		
		/*
		 * Editing the targetNamespace 
		 */
		bpel_scaner_obj.anonTargetNS(file_wsdl_edited, file_xsd_edited, file_bpel_edited, file_deploy_edited);
		
		System.out.println("Processes Completed");

	}
	
	

	/*
	 * Editing the imported file names  
	 */
	public void editImports(String file) throws GeneralException {

		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();

		Document document = utils_obj.documentBuilder(file);

		if(document.hasChildNodes()){
			utils_obj.editImports(document.getChildNodes());
			renamer_object.comitChanges(document, file);
		}

	}
	

}
