/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Registry {

	//File file_log_wsdl = new File(getFolder()+"wsdl_log_change.txt");
	//File file_log_bpel = new File(getFolder()+"bpel_log_change.txt");
	List<String> oldAttributes_bpel = new LinkedList<String>();
	List<String> newAttributes_bpel = new LinkedList<String>();
	LinkedList<String> nodeattributes = new LinkedList<String>();
	ArrayList<String> namespace = new ArrayList<String>();
	/*
	public String getFolder()
	{
		Main_controller bpel_parser_main = new Main_controller();

		return bpel_parser_main.getFolder();
	}
	 */


	//Commit changes and write the document content to the file specified

	/*
	 * edit the wsdl file
	 */

	public void editwsdlFile(Document document,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {
		for(int i=0 ; i<nodeattributes.size() ; i++) {
			if(document.hasChildNodes()){
				updateNodeAttribute_wsdl(document.getChildNodes(),nodeattributes.get(i),oldAttributes,newAttributes);
			}
		}
	}

	/*
	* Updates the attributes; changes the value of attributes and adds the old 
	* values to a list called oldAttributes and new values to a 
	* list called new Attributes
	* @param nodeList list of nodes of the WSDL
	* @param to_be_modifies name of the string to be modified
	* @param oldAttributes list of old attributes values
	* @param newAttributes list of new attribute values
	*/

	private void updateNodeAttribute_wsdl(NodeList nodeList, String to_be_modified,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {

		String randomstring=null;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(!tempNode.getNodeName().equals(namespace.get(0)+"definitions")){
					if(!tempNode.getParentNode().getNodeName().contains("binding")){
						if (tempNode.hasAttributes()) {
							NamedNodeMap nodeMap = tempNode.getAttributes();
							for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
								Node currentAttribute = nodeMap.item(i1);
								if(currentAttribute.getNodeValue().compareTo(to_be_modified)==0){
									if(!(tempNode.getNodeName().contains("fault") && currentAttribute.getNodeName().equals("name"))){
										if(tempNode.getNodeName().contains(":")){
											oldAttributes.add(tempNode.getNodeName().split(":")[1]+":"+to_be_modified);
										}
										else{
											oldAttributes.add(tempNode.getNodeName()+":"+to_be_modified);
										}

										ReNamer renamer_obj = new ReNamer();
										randomstring = renamer_obj.anonymizer();
										currentAttribute.setNodeValue(randomstring);
										newAttributes.add(randomstring);
									}
								}
								if(currentAttribute.getNodeValue().contains(":")){
									if(currentAttribute.getNodeValue().split(":")[1].compareTo(to_be_modified)==0){
										for (int j = 0; j < oldAttributes.size(); j++) {
											if(oldAttributes.get(j).split(":")[1].equals(to_be_modified) && 
													currentAttribute.getNodeName().equals(oldAttributes.get(j).split(":")[0])){
												currentAttribute.setNodeValue(currentAttribute.getNodeValue().split(":")[0]+":"+newAttributes.get(j));
											}
										}

									}

								}

							}
						}
					}

				}
				if (tempNode.hasChildNodes()) {
					updateNodeAttribute_wsdl(tempNode.getChildNodes(),to_be_modified,oldAttributes,newAttributes);
				}
			}
		}
	}




	/*
	 * called from WSDL_Scanner.findAttributes()
	 * Adds the attributes associated with a node to a list called nodeattributes
	 * @param nodeList list of nodes of the WSDL
	 * @param to_be_modifies name of the string to be modified
	 */

	public void addNodeAttribute(NodeList nodeList, String node_to_be_modified) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().equals(node_to_be_modified)){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(!currentAttribute.getNodeValue().contains(":")){
								nodeattributes.add(currentAttribute.getNodeValue());
							}

						}
					}
				}
			}
			if (tempNode.hasChildNodes()) {
				addNodeAttribute(tempNode.getChildNodes(),node_to_be_modified);
			}

		}

	}

	/*
	 * creates a linked list of critical nodes and adds the namespace prefix according to the wsdl file
	 * @param nodeList list of nodes of the WSDL
	 * @param critical_nodes list of critical nodes in the WSDL files
	 */
	public void createCriticalNodes(List<String> critical_nodes, NodeList nodelist) {
		
		Utilities util_obj = new Utilities();

		namespace.add("");
		namespace.add("");
		util_obj.matchNameSpacePrefix(nodelist, namespace);

		critical_nodes.add(namespace.get(1)+"partnerLinkType");
		critical_nodes.add(namespace.get(1)+"role");
		critical_nodes.add(namespace.get(0)+"fault");
		critical_nodes.add(namespace.get(0)+"input");
		critical_nodes.add(namespace.get(0)+"message");
		critical_nodes.add(namespace.get(0)+"operation");
		critical_nodes.add(namespace.get(0)+"output");
		critical_nodes.add(namespace.get(0)+"portType");
		critical_nodes.add(namespace.get(0)+"service");

	}

	/*
	 * Updates the attributes in bpel;
	 * @param nodeList list of nodes of the WSDL
	 * @param to_be_modified name of the string to be modified
	 * @param modified_to list of new attributes values
	 * @param namespace value of the namespace
	 */

	public void updateBpelNodeAttribute(NodeList nodeList, String to_be_modified, String modified_to,List<String> namespace) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(!tempNode.getNodeName().equals("bpel:process")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if((currentAttribute.getNodeValue().equals(to_be_modified.split(":")[1])) && 
									currentAttribute.getNodeName().toLowerCase().contains(to_be_modified.split(":")[0])){
								currentAttribute.setNodeValue(modified_to);
							}
							for (String ns : namespace) {
								if(currentAttribute.getNodeValue().compareTo(ns+to_be_modified.split(":")[1])==0 && 
										currentAttribute.getNodeName().equals(to_be_modified.split(":")[0])){
									currentAttribute.setNodeValue(ns+modified_to);
								}

								if(currentAttribute.getNodeValue().compareTo(ns+to_be_modified.split(":")[1])==0 &&
										currentAttribute.getNodeName().equals(to_be_modified.split(":")[0]+"Type")){
									currentAttribute.setNodeValue(ns+modified_to);
								}
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					updateBpelNodeAttribute(tempNode.getChildNodes(),to_be_modified,modified_to,namespace);
				}
			}
		}
	}

	/*
	 * updates the changes in wsdl from other wsdl 
	 * @param nodeList list of nodes of the WSDL
	 * @param oldAttributes list of old attributes values
	 * @param newAttributes list of new attribute values
	 */

	public void updatewsdlNodeAttribute_comparator(NodeList nodeList, String to_be_modified, String modified_to,List<String> namespace) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(!tempNode.getNodeName().equals("bpel:process")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							/*if((currentAttribute.getNodeValue().compareTo(to_be_modified.split(":")[1]))==0 ){
								currentAttribute.setNodeValue(modified_to);
							}*/
							if(!tempNode.getNodeName().contains("propertyAlias")){
							if(currentAttribute.getNodeValue().contains(":")){
								for (String ns : namespace) {
									if(currentAttribute.getNodeValue().compareTo(ns+to_be_modified.split(":")[1])==0 &&
											currentAttribute.getNodeName().toLowerCase().equals(to_be_modified.split(":")[0].toLowerCase())){
										currentAttribute.setNodeValue(ns+modified_to);
									}
								}
							}
						}

						}
					}
				}
				if (tempNode.hasChildNodes()) {
					updatewsdlNodeAttribute_comparator(tempNode.getChildNodes(),to_be_modified,modified_to,namespace);
				}
			}
		}
	}

	/*
	 *Matches the free element from the specified xml to the Bpel file
	 * @param nodeList list of nodes of the file
	 * @param nodename name of the node to be modified
	 * @param node_attribute value of the node attribute
	 */

	public void freeElementsFinder(NodeList nodeList,String node_name, String node_attribute) throws DOMException, GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().equals(node_name)){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();

						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

							Node currentAttribute = nodeMap.item(i1);

							if(currentAttribute.getNodeName().equals(node_attribute)){

								freeElementsModifier(nodeList, currentAttribute.getNodeValue(),node_name.split(":")[1],node_attribute);

							}

						}

					}
				}

			}


			if (tempNode.hasChildNodes()) {
				freeElementsFinder(tempNode.getChildNodes(), node_name, node_attribute);
			}

		}

	}


	/*
	 *Anonymises the free elements in the Bepl 
	 */

	public void freeElementsModifier(NodeList nodeList,String to_be_modified,String nodename, String attributename) throws GeneralException {
		String randomstring = null;
		ReNamer renamer_obj = new ReNamer();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

						Node currentAttribute = nodeMap.item(i1);	
						if(!tempNode.getNodeName().contains("propertyAlias")){
						if(currentAttribute.getNodeValue().equals(to_be_modified) &&
								(currentAttribute.getNodeName().equals(attributename) )){
							randomstring = renamer_obj.anonymizer();
							oldAttributes_bpel.add(to_be_modified);
							newAttributes_bpel.add(randomstring);
							if(currentAttribute.getNodeValue().contains(":")) {
								currentAttribute.setNodeValue(to_be_modified.split(":")[0]+":"+randomstring);
							}
							else {
								
								currentAttribute.setNodeValue(randomstring);
							}
						}
					}
					}

				}

			}

			if (tempNode.hasChildNodes()) {

				// loop again if has child nodes
				freeElementsModifier(tempNode.getChildNodes(), to_be_modified, nodename, attributename);

			}

		}
	}

	/*
	 * Propagates the free elements changes in the Bepl file with the new attribute values
	 */

	public void updateBpelFreeElements(NodeList nodeList, String to_be_modified, String modified_to) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

						Node currentAttribute = nodeMap.item(i1);

						if((currentAttribute.getNodeValue().compareTo(to_be_modified)==0)){
							if(currentAttribute.getNodeValue().contains(":")) {
								currentAttribute.setNodeValue(to_be_modified.split(":")[0]+":"+modified_to);
							}
							else {
								currentAttribute.setNodeValue(modified_to);
							}
						}

					}

				}

				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					updateBpelFreeElements(tempNode.getChildNodes(),to_be_modified,modified_to);

				}

			}

		}

	}

	/*
	 * Updates the WSDL file based on the anonimization done to thee XSD's
	 */
	public void updatewsdl_xsd(NodeList nodeList, String to_be_modified, String modified_to,String namespace) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(!tempNode.getNodeName().contains("propertyAlias")){
						if((currentAttribute.getNodeValue().equals(namespace+to_be_modified))) {
							currentAttribute.setNodeValue(namespace+modified_to);
						}
					}
					}
				}
				if (tempNode.hasChildNodes()) {
					updatewsdl_xsd(tempNode.getChildNodes(),to_be_modified,modified_to,namespace);
				}
			}
		}
	}



}


