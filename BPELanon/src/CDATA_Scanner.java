import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class CDATA_Scanner {

	//List<String> namespace = null;
	String element = "";
	List<String> oldBpelvar1 = new LinkedList<String>();
	Map<String ,String> varible_message = new HashMap<String,String>();
	Map<String,String> name_msgType = new HashMap<String,String>();
	List<String> newbpelvar1 = new LinkedList<String>();
	List<String> attributes = new LinkedList<>();
	List<String> elements_xsd = new LinkedList<>();
	Map<String, String> namespace = new HashMap<String, String>();
	List<String> freevariables  = new LinkedList<String>();
	Map<String, String> cdata_variables  = new HashMap<String, String>();
	
	
	/*
	 * used to find the name space of xsc files
	 * @param nodeList list of nodes of XSD file
	 */
	public void nameSpacefinder(NodeList nodeList){
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeName().startsWith("xmlns:") && 
								(tempNode.getNodeName().contains("process") || tempNode.getNodeName().contains("definitions")
										|| tempNode.getNodeName().contains("schema"))){
							namespace.put(currentAttribute.getNodeName().substring(6, currentAttribute.getNodeName().length()),
									currentAttribute.getNodeValue().substring(0, currentAttribute.getNodeValue().length()));
						}
					}


				}
				if (tempNode.hasChildNodes()) {
					nameSpacefinder(tempNode.getChildNodes());
				}
			}
		}
	}
/*
 * Used to finf the element name by matching against then message name
 * @param nodeList list of nodes of the WSDL
 * @param msg_name value of the message node
 */
	public void wsdlElementFinder(NodeList nodeList,String msg_name){
		Boolean flag = false;
		String temp="";
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					if(tempNode.getParentNode().getNodeName().toLowerCase().contains("message")){
						NamedNodeMap parentNodeMap = tempNode.getParentNode().getAttributes();
						for (int i1 = 0; i1 < parentNodeMap.getLength(); i1++) {
							Node currentparentAttribute = parentNodeMap.item(i1);
							if(currentparentAttribute.getNodeValue().endsWith(msg_name)){
								temp= currentparentAttribute.getNodeValue();
								flag = true;
							}
						}
						if(flag){
							for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

								Node currentAttribute = nodeMap.item(i1);
								if(currentAttribute.getNodeName().equals("element")){
									element = currentAttribute.getNodeValue().split(":")[1];
									varible_message.put(element, temp);
									flag = false;
								}
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					wsdlElementFinder(tempNode.getChildNodes(),msg_name);
				}
			}
		}
	}

/*
 * Used to find the find the attribute vale matching against the element name from the WSDl
 * @param nodeList list of nodes of XSD
 */
	public void xsdAttributeFinder(NodeList nodeList){
		Boolean flag = false;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				//System.out.println(tempNode.getNodeName());
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					if(tempNode.getParentNode().hasAttributes() || tempNode.getParentNode().getNodeName().contains("sequence")){
						NamedNodeMap parentnodeMap = tempNode.getParentNode().getAttributes();

						if(tempNode.getParentNode().getNodeName().contains("sequence")){
							for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
								Node currentAttribute = nodeMap.item(i1);
								if(tempNode.getNodeName().contains("element")){
									if(currentAttribute.getNodeName().equals("name")){
										//System.out.println(currentAttribute.getNodeValue());
										attributes.add(currentAttribute.getNodeValue());
										//System.out.println("att");
									}
								}
							}
						}
						else{
							for (int j1 = 0; j1 < parentnodeMap.getLength(); j1++) {
								Node currentparentAttribute = parentnodeMap.item(j1);
								if(currentparentAttribute.getNodeName().equals("name") && currentparentAttribute.getNodeValue().contains(element)
										&& tempNode.getParentNode().getNodeName().contains("complexType")){
									flag = true;
								}

							}
						}
						if(flag){
							String name = "";
							String type = "";
							for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
								Node currentAttribute = nodeMap.item(i1);
								if(tempNode.getNodeName().equals("attribute")){
									if(currentAttribute.getNodeName().equals("name")){
										//System.out.println(currentAttribute.getNodeValue());
										name= currentAttribute.getNodeValue();
										attributes.add(currentAttribute.getNodeValue());
										//System.out.println("att");
									}
									if(currentAttribute.getNodeName().equals("type")){
										type= currentAttribute.getNodeValue();
									}
									flag = false;
								}
							}
						//	System.out.println(type);
							elements_xsd.add(name+type.split(":")[1]);
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					xsdAttributeFinder(tempNode.getChildNodes());
				}
			}
		}
	}

	/*
	 * Used to match the appropriate XSD with the WSDL file
	 * @param msgtype value of the messageType attribute
	 * @param wsdl_file WSDL file name
	 * @param xsd_file name of the XSD file
	 * @param var_name value of the variable name attribute
	 */
	public void fileMatcher(String msgtype,List<String> wsdl_file , List<String> xsd_file, String var_name){
		Utilities utils_obj = new Utilities();
		Document document_wsdl = null;
		Document document_xsd = null;
		String ns_prefix = "";
		String ns = "";

		try{
			ns_prefix = msgtype.split(":")[0];
			ns  = namespace.get(ns_prefix);
		}
		catch(Exception e){
			System.out.println("error in msg type");
		}
		@SuppressWarnings("unused")
		String file_wsdl = "";
		@SuppressWarnings("unused")
		String file_xsd = "";

		for (String wsdl : wsdl_file) {
			document_wsdl = utils_obj.documentBuilder(wsdl);
			String targetns = utils_obj.getTargetNameSpace(document_wsdl.getChildNodes());
			if(targetns.equals(ns)){
				file_wsdl = wsdl;
				break;
			}
		}

		for (String xsd : xsd_file) {
			document_xsd = utils_obj.documentBuilder(xsd);
			String targetns = utils_obj.getTargetNameSpaceXSD(document_xsd.getChildNodes());
			if(targetns.equals(ns)){
				file_xsd = xsd;
				break;
			}
		}

		wsdlElementFinder(document_wsdl.getChildNodes(),msgtype.split(":")[1]);

		if(document_xsd != null){
			xsdAttributeFinder(document_xsd.getChildNodes());
		}

		Integer temp1 = attributes.size();
		String temp = temp1.toString();
		for (String attr : attributes) {
			temp = temp + ":"+ attr;
		}
		freevariables.add(var_name);
		cdata_variables.put(var_name, temp);
		attributes.clear();
		temp = "";
		element="";

	}

	/*
	 * calls the cdata mapper
	 * @param file BPEL file name
	 * @param oldCDATA values of old CDATA attributes
	 * @param newCDATA values of the new CDATA attributes
	 */
	public void xsdAttributeMapper(String file, List<String> oldCDATA , List<String> newCDATA) throws GeneralException {


		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();

		Document document = utils_obj.documentBuilder(file);
		testcdata(document.getChildNodes(), oldCDATA, newCDATA);
		renamer_object.comitChanges(document, file);
	
	}


/*
 * copies the values to global variables
 */
	public void bpelvarCopier( List<String> oldBpelvar , List<String> newBpelvar) {
		oldBpelvar1.addAll(oldBpelvar);
		newbpelvar1.addAll(newBpelvar);
	}

	/*
	 * used to identify all the CDATA varible in the BPEL file
	 * @param nodeList lest of BPEL nodes
	 * @param wsdl_file name of the WSDL file
	 * @param xsd_file name of the xsd file
	 */
	public void bpelVariableScanner(NodeList nodeList,List<String> wsdl_file , List<String> xsd_file) throws DOMException, GeneralException {

		String name = "";
		String msgtype= "";
		//Registry reg_obj= new Registry();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					if(tempNode.getNodeName().toLowerCase().contains("variable")){
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

							Node currentAttribute = nodeMap.item(i1);
							if(tempNode.getNodeName().toLowerCase().contains("variable")){
								if(currentAttribute.getNodeName().equals("name")){
									name= currentAttribute.getNodeValue();
								}
								if(currentAttribute.getNodeName().equals("messageType")){
									msgtype = currentAttribute.getNodeValue();
								}

							}
						}
						name_msgType.put(msgtype.split(":")[1], name);
					//	System.out.println("NAMESPACES @@@ "+namespace);
						fileMatcher(msgtype,wsdl_file,xsd_file, name);
					}

				}
				if (tempNode.hasChildNodes()) {
					bpelVariableScanner(tempNode.getChildNodes(),wsdl_file, xsd_file);
				}
			}
		}
	}


/*
 * used to find all the CDATA values 
 * @param nodeList list of nodes of BPEL file
 * @param oldCDATA list of old CDATA values
 * @param newCDATA list of new CDAT values
 */
	public void testcdata(NodeList nodeList,List<String> oldCDATA, List<String> newCDATA) throws GeneralException {
		//String temp = null;
		String tempparent = null;
		String replacement_value = "";
		Boolean temp_flag =false;
	//	int l =0;
	//	int start = 0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.CDATA_SECTION_NODE) {
				//System.out.println(tempNode.getNodeValue());
				String cdata_value = tempNode.getNodeValue();
			//	System.out.println("val: : "+cdata_value);
				if(cdata_value.contains("$")){
				//	System.out.println("$$$");
				}
				NamedNodeMap nodeMapparent = tempNode.getParentNode().getParentNode().getAttributes();
				for (int i1 = 0; i1 < nodeMapparent.getLength(); i1++) {
					Node parentAttribute = nodeMapparent.item(i1);
					if(parentAttribute.getNodeName().equals("variable")){
						tempparent = parentAttribute.getNodeValue();
					}
				}

				if(tempparent!=null){
					String attr_values = cdata_variables.get(tempparent);
					List<String> temp_attr =new LinkedList<String>();
					for (int j = 1; j <= Integer.parseInt(attr_values.split(":")[0]); j++) {
						temp_attr.add(attr_values.split(":")[j]);
					}

					int k =0;
					for (String attr : temp_attr) {
						k=0;
						for (String newcdata : newCDATA) {
							String oldcdata = oldCDATA.get(k++);
							if(newcdata.equals(attr)){
								if(oldcdata.equals(cdata_value)){
							//		System.out.println("replace    :"+cdata_value +" ************************************  :"+newcdata);
									replacement_value = newcdata;
									tempNode.setNodeValue(replacement_value);
								}	
							}
						}
					}
				}

				if(cdata_value.contains(":")||cdata_value.contains("@")||cdata_value.contains("/")){
					String final_value =cdata_value;
					int count = 0;
					//Boolean element_flag = false;
					if(cdata_value.contains(":")){
						for (int l1 = 0; l1 < cdata_value.length(); l1++){
							if (cdata_value.charAt(l1) == ':')
								count++;
						}
						for (int j1 = 0; j1 < count; j1++) {
							String temporary = cdata_value.split(":")[j1];
							String temp1 = cdata_value.split(":")[j1+1];
							String temp2 = "";
							Integer length = 0;
							for (int j = 0; j < temp1.length(); j++) {
								if(temp1.charAt(j)=='/' || temp1.charAt(j)=='@' ||temp1.charAt(j)=='['){
									length = j;
									break;
								}
								else{
									temp2 = temp2+temp1.charAt(j);
								}
							}
							if(length==0){
								length = temp1.length();
							}
							if(tempparent!=null){
								String attr_values = cdata_variables.get(tempparent);
								List<String> temp_attr =new LinkedList<String>();
								for (int j = 1; j <= Integer.parseInt(attr_values.split(":")[0]); j++) {
									temp_attr.add(attr_values.split(":")[j]);
								}
								if(temp_attr.size()==0){
									temp_flag=true;
								}
								int k =0;
								for (String attr : temp_attr) {
									k=0;
									for (String newcdata : newCDATA) {
										String oldcdata = oldCDATA.get(k++);
										if(newcdata.equals(attr)){
											if(oldcdata.equals(temp2)){
											//	System.out.println("replace    :"+temp2 +" ************************************  :"+newcdata);
												replacement_value = newcdata;
												final_value = temporary+":"+newcdata+cdata_value.split(":")[j1+1].substring(length);
												//element_flag = true;
											}	
										}
									}
								}
							}
							if(tempparent==null || temp_flag==true){
								temp_flag = false;
								int k =0;
								for (String newcdata : newbpelvar1) {
									String oldcdata = oldBpelvar1.get(k++);
									if(oldcdata.equals(temp2)){
									//	System.out.println("replace    :"+temp2 +" ************************************  :"+newcdata);
										replacement_value = newcdata;
										if(j1!=0){
											final_value+=":";
										}
										if(j1==0){
											final_value = "";
										}
										final_value +=  temporary+":"+newcdata+cdata_value.split(":")[j1+1].substring(length);
										break;
										//element_flag = true;
									}	

								}
							}
						}
					}
					if(cdata_value.contains("/")){
						String temporary = cdata_value.split("/")[0];
						String temp1 = cdata_value.split("/")[1];
						String temp2 = "";
						Integer length = 0;
						for (int j = 0; j < temp1.length(); j++) {
							if(temp1.charAt(j)==':' || temp1.charAt(j)=='@'  ||temp1.charAt(j)=='['){
								length = j;
								break;
							}
							else{
								temp2 = temp2+temp1.charAt(j);	
							}
						}
						if(length==0){
							length = temp1.length();
						}
						if(tempparent!=null){
							String attr_values = cdata_variables.get(tempparent);
							List<String> temp_attr =new LinkedList<String>();
							for (int j = 1; j <= Integer.parseInt(attr_values.split(":")[0]); j++) {
								temp_attr.add(attr_values.split(":")[j]);
							}

							int k =0;
							for (String attr : temp_attr) {
								k=0;
								for (String newcdata : newCDATA) {
									String oldcdata = oldCDATA.get(k++);
									if(newcdata.equals(attr)){
										if(oldcdata.equals(temp2)){
										//	System.out.println("replace    :"+temp2 +" ************************************  :"+newcdata);
											replacement_value = newcdata;
											final_value = temporary+"/"+newcdata+cdata_value.split("/")[1].substring(length);
										}	
									}
								}
							}
						}

					}
					if(cdata_value.contains("@")){
						String temporary = cdata_value.split("@")[0];
						String temp1 = cdata_value.split("@")[1];
						String temp2 = "";
						Integer length = 0;
						for (int j = 0; j < temp1.length(); j++) {
							if(temp1.charAt(j)==':' || temp1.charAt(j)=='/' || temp1.charAt(j)=='\0' ||temp1.charAt(j)=='[' ){
								length = j;
								break;
							}
							else{
								temp2 = temp2+temp1.charAt(j);
							}
						}
						if(length==0){
							length = temp1.length();
						}
						if(tempparent!=null){
							String attr_values = cdata_variables.get(tempparent);
							List<String> temp_attr =new LinkedList<String>();
							for (int j = 1; j <= Integer.parseInt(attr_values.split(":")[0]); j++) {
								temp_attr.add(attr_values.split(":")[j]);
							}

							int k =0;
							for (String attr : temp_attr) {
								k=0;
								for (String newcdata : newCDATA) {
									String oldcdata = oldCDATA.get(k++);
									if(newcdata.equals(attr)){
										if(oldcdata.equals(temp2)){
										//	System.out.println("replace    :"+temp2 +" ************************************  :"+newcdata);
											replacement_value = newcdata;
											final_value = temporary+"@"+newcdata+cdata_value.split("@")[1].substring(length);
										}	
									}
								}
							}
						}

					}

					tempNode.setNodeValue(final_value);
				}


			}
			if (tempNode.hasChildNodes()) {
				testcdata(tempNode.getChildNodes(),oldCDATA,newCDATA);

			}
		}
	}
	/*
	 * Used to find the map CADATA values starting with $
	  * @param nodeList list of nodes of BPEL file
	  * @param oldCDATA list of old CDATA values
	  * @param newCDATA list of new CDAT values
	 */
	public void cdataspecial(NodeList nodeList,List<String> oldCDATA, List<String> newCDATA) throws GeneralException {
		String temp = null;
		int l =0;
		int start = 0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.CDATA_SECTION_NODE) {
				String value = tempNode.getNodeValue();
				for (int j = 0; j < value.length(); j++) {
					temp=null;
					if(value.charAt(j)=='$'){
						j++;
						start = j;
						while(Character.isLetter(value.charAt(j))){
							temp=temp+value.charAt(j++);
						}
						temp=temp.substring(4);
						l=0;
						for (String li : oldCDATA) {
							String new_varible = newCDATA.get(l++);
							if(temp.equals(li)){
								value= value.substring(0, start)+new_varible+value.substring(start+temp.length());
								tempNode.setNodeValue(value);
								break;
							}
						}
					}

				}
			}
			if (tempNode.hasChildNodes()) {
				cdataspecial(tempNode.getChildNodes(),oldCDATA,newCDATA);

			}

		}
	}

	/*
	 * used to update the CDATA vlues with corelation nodes 
	 * @param nodeList list of nodes of the WSDL
	 * @param oldCDATA list of old CDATA values
	 * @param newCDATA list of new CDAT values
	 */
	public void updateCorelationCDATA(NodeList nodeList,List<String> oldCDATA,List<String> newCDATA) throws GeneralException {
		String temp=null;
		Boolean flag = false;
		//int j=0;

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.CDATA_SECTION_NODE) {
				if(tempNode.getParentNode().getParentNode().getNodeName().contains("propertyAlias") ){

					if(tempNode.getParentNode().getParentNode().getNodeName().toLowerCase().contains("propertyalias")){
						NamedNodeMap parentNodeMap = tempNode.getParentNode().getParentNode().getAttributes();
						for (int i1 = 0; i1 < parentNodeMap.getLength(); i1++) {
							Node currentparentAttribute = parentNodeMap.item(i1);
							if(currentparentAttribute.getNodeName().contains("messageType")){
								temp= currentparentAttribute.getNodeValue().split(":")[1];
								flag = true;
							}
						}
						if(flag){
							String cdata = tempNode.getNodeValue();
							try{
								cdata = cdata.split("/")[1];
							}
							catch(Exception e){
								System.out.println("Corelation Cdata format error" + e.getMessage());
							}
							String msgname = name_msgType.get(temp);
							String attr_values = cdata_variables.get(msgname);
							List<String> temp_attr =new LinkedList<String>();
							for (int j1 = 1; j1 <= Integer.parseInt(attr_values.split(":")[0]); j1++) {
								temp_attr.add(attr_values.split(":")[j1]);
							}

							int k =0;
							for (String attr : temp_attr) {
								k=0;
								for (String newcdata : newCDATA) {
									String oldcdata = oldCDATA.get(k++);
									if(newcdata.equals(attr)){
										if(oldcdata.equals(cdata)){
										//	System.out.println("replace    :"+cdata +" ************************************  :"+newcdata);
											tempNode.setNodeValue("/"+newcdata);
										}	
									}
								}
							}
							
						}
					
					}

				}
			}
			if (tempNode.hasChildNodes()) {

				// loop again if has child nodes
				updateCorelationCDATA(tempNode.getChildNodes(),oldCDATA,newCDATA);

			}
		}


	}
	
	
	/*
	 * anonymizes the string constant values in cdata
	 * 
	 */
	public void anonString(String file_bpel) throws GeneralException{
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		
		Document document_bpel = utils_obj.documentBuilder(file_bpel);
		
		NodeList nodeList = document_bpel.getChildNodes();
		
		editString(nodeList);
		
		
		renamer_object.comitChanges(document_bpel, file_bpel);
	}
	
	/*
	 * edits the string constant values in cdata
	 * 
	 */
	public void editString(NodeList nodeList) throws GeneralException{
		
		int j = 0;
		String[] seperator = new String[2];
		
		ReNamer renamer_object = new ReNamer();
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.CDATA_SECTION_NODE) {
				String cdata_value = tempNode.getNodeValue();
				if(cdata_value.contains("'")){
					seperator[j++] = "'";
				}
//				if(cdata_value.contains("\"")){
//					seperator[j++] = "\"";
//					System.out.println("there is a \"");
//					
//				}
				for(int i2 = 0; i2 < j; i2++){
					String[] splitted = cdata_value.split(seperator[i2]);
					cdata_value = splitted[0];
					for(int i3 = 2; i3 < splitted.length; i3 += 2){
						cdata_value = cdata_value + seperator[i2] + renamer_object.anonymizer() + seperator[i2] + splitted[i3];
					}
				}
				tempNode.setNodeValue(cdata_value);
			}
			if(tempNode.hasChildNodes()){
				editString(tempNode.getChildNodes());
			}
		}
	}


}



