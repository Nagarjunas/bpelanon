/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.util.Iterator;
import java.util.LinkedList;


import java.util.List;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class WSDL_Scanner {


	List<String> namespaceprefix = new LinkedList<String>();
	List<String> namespace = new LinkedList<String>();
	List<String> newfaultnames = new LinkedList<String>();
	LinkedList<String> registry = new LinkedList<String>();
	TreeSet<String> oldfaultnames = new TreeSet<String>();		


	public void wsdlScanner(TreeSet<String> nodeNames,String file){

		Utilities utils_obj = new Utilities();
		Document document = utils_obj.documentBuilder(file);

		if(document.hasChildNodes()){
			getNode(document.getChildNodes(),nodeNames);


		}

	}

    /*
	* getnode function gets the critical nodes in the wsdl files
	* @param nodelist nodelist of the wsdl file
	* @param nodename name of the nodes to be modified
	*/

	private void getNode(NodeList nodeList,TreeSet<String> nodeNames) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				nodeNames.add(tempNode.getNodeName());

				if (tempNode.hasChildNodes()) {
					getNode(tempNode.getChildNodes(),nodeNames);
				}				
			}
		}
	}

	/*
	* finds the attributes associated with the critical nodes
	* @param list list of critical node names
	* @param file name of the wsdl file
	* @param oldattributes list to save old attrribute values
	* @param new attributes list to save the new attributre values 
	*/


	public void findAttributes(TreeSet<String> list,String file,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {

		Utilities utils_obj = new Utilities();
		Document document = utils_obj.documentBuilder(file);
		Registry registry = new Registry();
		ReNamer renamer_object = new ReNamer();
		List<String> critical_nodes = new LinkedList<>();
		String randomstring = null;

		if(document.hasChildNodes()){
			registry.createCriticalNodes(critical_nodes,document.getChildNodes());
		}
		for (String li : list) {
			for (String li1 : critical_nodes) {
				if(li.equals(li1)){
					if(document.hasChildNodes()){
						registry.addNodeAttribute(document.getChildNodes(),li);
					}
				}
			}

		}	
		registry.editwsdlFile(document,oldAttributes,newAttributes);
		registry.nodeattributes.clear();


		renamer_object.comitChanges(document, file);

		if(document.hasChildNodes()){
			updateBinding_wsdl(document.getChildNodes(), oldAttributes, newAttributes);
			findfaultvaribles_wsdl(document.getChildNodes(),oldfaultnames);
			for (String oldfaultname : oldfaultnames) {
				ReNamer renamer_obj = new ReNamer();
				randomstring = renamer_obj.anonymizer();
				newfaultnames.add(randomstring);
				updatefaultvaribles_wsdl(document.getChildNodes(), oldfaultname, randomstring);
			}

		}

		renamer_object.comitChanges(document, file);		//commit changes to the file

	}

	/*
	* compares the wsdl files with each other , so that the anonymization within 
	* each file is reflected in the other wsdl files also
	* @param file name of the wsdl file
	* @param name of the wsdl file needed to be compared
	* @param oldattributes list of old attribute values
	* @param newattributes list of new attributes values
	*/

	public void wsdlCompararer(String file,String file_wsdl,List<String> oldAttributes,List<String> newAttributes) {

		ReNamer renamer_object = new ReNamer();
		String targetnamespace =null;
		Utilities utils_obj = new Utilities();
		Registry reg_obj = new Registry();
		Document document = utils_obj.documentBuilder(file);
		Document document_wsdl = utils_obj.documentBuilder(file_wsdl);
		if(document_wsdl.hasChildNodes()){
			targetnamespace = utils_obj.getTargetNameSpace(document_wsdl.getChildNodes());
		}

		if(document.hasChildNodes()){
			namespace.clear();
			utils_obj.findNameSpacePrefix(document.getChildNodes(),targetnamespace,namespace);
		}

		if(document.hasChildNodes()){
			for (int i = 0; i < oldAttributes.size(); i++) {
				reg_obj.updatewsdlNodeAttribute_comparator(document.getChildNodes(),oldAttributes.get(i),newAttributes.get(i),namespace);

			}	

			renamer_object.comitChanges(document, file);
			renamer_object.comitChanges(document_wsdl, file_wsdl);

		}

	}

	/*
	 * update the changes to the wsdl binding section
	 * @param nodelist nodelist of the wsdl file
	 *  @param oldattributes list of old attribute values
	 * @param newattributes list of new attributes values
	 */

	public void updateBinding_wsdl(NodeList nodeList,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {
		String modified_to=null;
		int j=0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("binding")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							j=0;
							for (String oldlist : oldAttributes) {
								modified_to=newAttributes.get(j++);
								if(currentAttribute.getNodeValue().compareTo("tns:"+oldlist.split(":")[1])==0 
										&& ("port"+currentAttribute.getNodeName()).compareTo(oldlist.split(":")[0].toLowerCase())==0 ){ 
									currentAttribute.setNodeValue("tns:"+modified_to);
								}
							}


						}
					}
				}
				if(tempNode.getNodeName().contains("operation")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							j=0;
							for (String oldlist : oldAttributes) {
								modified_to=newAttributes.get(j++);
								if(currentAttribute.getNodeValue().compareTo(oldlist.split(":")[1])==0 
										&& (tempNode.getNodeName()).contains(oldlist.split(":")[0].toLowerCase()) ){
									currentAttribute.setNodeValue(modified_to);
								}
							}


						}
					}
				}
				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					updateBinding_wsdl(tempNode.getChildNodes(),oldAttributes,newAttributes);

				}
			}
		}
	}

	/*
	 * finds the list of fault variables in the wsdl
	 * @param nodelist nodelist of the wsdl file
	 * @param oldfaultnames list of old fault values
	 */
	public void findfaultvaribles_wsdl(NodeList nodeList,TreeSet<String> oldfaultnames) throws GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("fault")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(currentAttribute.getNodeName().compareTo("name")==0) { 
								oldfaultnames.add(currentAttribute.getNodeValue());
							}
						}
					}
				}

				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					findfaultvaribles_wsdl(tempNode.getChildNodes(),oldfaultnames);

				}
			}
		}
	}

	/*
	 * anonymizes the fault variables
	 * @param nodelist nodelist of the wsdl file
	 * @param oldfaultnames list of old fault values
	 * @param newfaultnames list of old fault values
	 */

	public void updatefaultvaribles_wsdl(NodeList nodeList,String oldfaultname,String newfaultname) throws GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("fault")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(currentAttribute.getNodeName().compareTo("name")==0 && currentAttribute.getNodeValue().compareTo(oldfaultname)==0) { 
								currentAttribute.setNodeValue(newfaultname);
							}
						}
					}
				}

				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					updatefaultvaribles_wsdl(tempNode.getChildNodes(),oldfaultname,newfaultname);

				}
			}
		}
	}

	/*
	 * Edit the deployment descriptor to update the changes made in the Wsdl files
	 * @param file name of the deployment descriptor
	 * @param oldattributes list to save old attrribute values
	 * @param new attributes list to save the new attributre values 
	 */
	public void editDeploy(String file,List<String> oldAttributes , List<String> newAttributes) throws GeneralException {
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		Document document = utils_obj.documentBuilder(file);


		if(document.hasChildNodes()){
			editDeploymentDescriptor(document.getChildNodes(), oldAttributes, newAttributes);
			renamer_object.comitChanges(document, file);
		}
	}

	/*
	* Recursive function Edit the deployment descriptor 
	* @param nodelist list of nodes of the deployment descriptor
	* @param oldattributes list to save old attribute values
	* @param new attributes list to save the new attribute values 
	*/

	public void editDeploymentDescriptor(NodeList nodeList,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {
		int j=0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("service")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							j=0;
							for (String oldAttribute : oldAttributes) {
								String newAttribute = newAttributes.get(j++);
								if(currentAttribute.getNodeName().compareTo("name")==0  && 
										(currentAttribute.getNodeValue().split(":")[1].equals(oldAttribute.split(":")[1])
												&& tempNode.getNodeName().contains(oldAttribute.split(":")[0]))) { 
									currentAttribute.setNodeValue(currentAttribute.getNodeValue().split(":")[0]+":"+newAttribute);
								}
							}

						}
					}
				}

				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					editDeploymentDescriptor(tempNode.getChildNodes(),oldAttributes,newAttributes);

				}
			}
		}
	}

	/*
	 * Updates the Corelation
	 * @param nodeList list of nodes of the WSDL file
	 * @param oldattributes list to save old attribute values
	 * @param new attributes list to save the new attribute values 
	 */
	public void updateCorelation(NodeList nodeList,List<String> oldAttributes,List<String> newAttributes) throws GeneralException {
		String modified_to=null;
		int j=0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("propertyAlias")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							j=0;
							for (String oldlist : oldAttributes) {
								modified_to=newAttributes.get(j++);
								//System.out.println(oldlist);
								if(currentAttribute.getNodeName().contains("message")){
								if(currentAttribute.getNodeValue().split(":")[1].compareTo(oldlist.split(":")[1])==0 
										&& (currentAttribute.getNodeName()).compareTo(oldlist.split(":")[0]+"Type")==0 ){ 
									//System.out.println("*****"+currentAttribute.getNodeValue());
									currentAttribute.setNodeValue(currentAttribute.getNodeValue().split(":")[0]+":"+modified_to);
									break;
								}
							}
							}


						}
					}
				}
				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					updateCorelation(tempNode.getChildNodes(),oldAttributes,newAttributes);

				}
			}
		}
	}

	/*
	 * Updates the Corelation property
	 */
	public void updateCorelationProp(String file_wsdl, String file_bpel) throws GeneralException {
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		
		Document document_wsdl = utils_obj.documentBuilder(file_wsdl);
		Document document_bpel = utils_obj.documentBuilder(file_bpel);
		
		List<String> oldproperty = new LinkedList<String>();
		List<String> newproperty = new LinkedList<String>();
		
		if(document_wsdl.hasChildNodes()) {
			getwsdlproperty(document_wsdl.getChildNodes(), oldproperty, newproperty);
			editwsdlproperty(document_wsdl, oldproperty, newproperty);
		}
		
		renamer_object.comitChanges(document_wsdl, file_wsdl);
		
		editbpelproperty(document_bpel.getChildNodes(), oldproperty, newproperty);
		
		renamer_object.comitChanges(document_bpel, file_bpel);
		
	}
	
	/*
	 * gets the Corelation property in wsdl and used in updateCorelationProp()
	 */
	public void getwsdlproperty(NodeList nodeList, List<String> oldproperty, List<String> newproperty) throws GeneralException {
		String randomstring = null;
		ReNamer renamer_obj = new ReNamer();
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().equals("vprop:property")) {
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(currentAttribute.getNodeName().equals("name")) {
								oldproperty.add(currentAttribute.getNodeValue());
								randomstring = renamer_obj.anonymizer();
								newproperty.add(randomstring);
								currentAttribute.setNodeValue(randomstring);
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					getwsdlproperty(tempNode.getChildNodes(), oldproperty, newproperty);
				}
			}
		}
	}
	
	/*
	 * edits the Corelation property in wsdl and used in updateCorelationProp()
	 */
	public void editwsdlproperty(Document document_wsdl, List<String> oldproperty, List<String> newproperty){
		
		NodeList nodeList = document_wsdl.getElementsByTagName("vprop:propertyAlias");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.hasAttributes()) {
				NamedNodeMap nodeMap = tempNode.getAttributes();
				for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
					Node currentAttribute = nodeMap.item(i1);
					if(currentAttribute.getNodeName().equals("propertyName")) {
						for(int i2 = 0; i2 < oldproperty.size(); i2++) {
							if(currentAttribute.getNodeValue().split(":")[1].equals(oldproperty.get(i2))){
								currentAttribute.setNodeValue(currentAttribute.getNodeValue().split(":")[0]+":"+newproperty.get(i2));
								break;
							}
						}
					}
				}
			}
		}
	}
	
	/*
	 * edits the Corelation property in bpel and used in updateCorelationProp()
	 */
	public void editbpelproperty(NodeList nodeList, List<String> oldproperty, List<String> newproperty) {
		String tempNodename = "";
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains(":")){
					tempNodename = tempNode.getNodeName().split(":")[1];
				}
				else {
					tempNodename = tempNode.getNodeName();
				}
				if(tempNodename.equals("correlationSet")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(currentAttribute.getNodeName().equals("properties")) {
								for(int i2 = 0; i2 < oldproperty.size(); i2++) {
									if(currentAttribute.getNodeValue().split(":")[1].equals(oldproperty.get(i2))){
										currentAttribute.setNodeValue(currentAttribute.getNodeValue().split(":")[0]+":"+newproperty.get(i2));
										break;
									}
								}
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					editbpelproperty(tempNode.getChildNodes(), oldproperty, newproperty);
				}
			}
		}
		
	}
	
}

