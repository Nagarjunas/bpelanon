/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;


public class UI_DirectoryChooser extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String folder;
	JButton jButton1;
	JButton jButton2;
	JButton jButton3;
	JLabel jLabel1;
	JLabel jLabel2;
	JLabel jLabel3;
	JTextField jTextField1;  
	JFileChooser chooser;
	String choosertitle;

	public UI_DirectoryChooser(Container pane) {

		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));		
		jButton1 = new JButton("Select Project Directory");
		jButton1.setToolTipText("Select the project folder which is needed to be anonymized");
		jButton1.addActionListener(this);
		jButton1.setActionCommand("select path");
		jButton1.setAlignmentX(Component.CENTER_ALIGNMENT);

		jButton2 = new JButton("Anonymize");
		jButton2.setToolTipText("Select the project folder first!");
		jButton2.addActionListener(this);
		jButton2.setActionCommand("anonymize");
		jButton2.setEnabled(false);
		jButton2.setAlignmentX(Component.CENTER_ALIGNMENT);

		jButton3 = new JButton("Delete Originals");
		jButton3.setToolTipText("Select the project folder first!");
		jButton3.addActionListener(this);
		jButton3.setActionCommand("delete");
		jButton3.setEnabled(false);
		jButton3.setAlignmentX(Component.CENTER_ALIGNMENT);

		jLabel1 = new JLabel("BPELanon");
		jLabel1.setAlignmentX(Component.CENTER_ALIGNMENT);
		jLabel1.setFont(new Font("Serif", Font.BOLD,14));
		jLabel2 = new JLabel("");
		jLabel2.setAlignmentX(Component.CENTER_ALIGNMENT);
		jLabel3 = new JLabel("");
		jLabel3.setAlignmentX(Component.CENTER_ALIGNMENT);

		pane.add(Box.createRigidArea(new Dimension(0,20)));
		pane.add(jLabel1);
		pane.add(Box.createRigidArea(new Dimension(5,10)));
		pane.add(jButton1);
		pane.add(Box.createRigidArea(new Dimension(5,10)));
		pane.add(jLabel3);
		//pane.add(Box.createRigidArea(new Dimension(5,10)));
		pane.add(jButton3);
		pane.add(jLabel2);
		pane.add(Box.createRigidArea(new Dimension(5,10)));
		pane.add(jButton2);
	}


	public String getFolder() {
		return folder;
	}


	public void setFolder(String folder) {
		this.folder = folder;
	}


	public void actionPerformed(ActionEvent e) {


		if("select path".equals(e.getActionCommand())){
			chooser = new JFileChooser(); 
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(choosertitle);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			//
			// disable the "All files" option.
			//
			chooser.setAcceptAllFileFilterUsed(false);
		
			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
				jLabel2.setText(" ");
				jLabel3.setText(" ");
				setFolder(chooser.getSelectedFile().toString());
				jButton2.setEnabled(true);
				jButton3.setEnabled(true);
				jButton3.setToolTipText("Click to delete project files");
				jButton2.setToolTipText("Click to start anonymization of the project");
			}
			else {
				jLabel2.setText("");
				jLabel3.setText("No Selection!! ");
			}
		}
		if("delete".equals(e.getActionCommand())){
			int option = JOptionPane.showConfirmDialog(getParent(), "Delete original project files from folder");
			if(option == JOptionPane.YES_OPTION){
				Utilities util_obj = new Utilities();
				File wsdl[] = util_obj.finder(folder,"wsdl");
				File bpel[] = util_obj.finder(folder,"bpel");
				File xsd[]  = util_obj.finder(folder,"xsd");
				File deploy[] = util_obj.finder(folder, "xml");

				for (int i = 0; i < wsdl.length; i++) {
					wsdl[i].delete();
				}
				for (int i = 0; i < bpel.length; i++) {
					bpel[i].delete();
				}
				for (int i = 0; i < xsd.length; i++) {
					xsd[i].delete();
				}
				for (int i = 0; i < deploy.length; i++) {
					deploy[i].delete();
				}
				jLabel2.setText("Files Deleted!");
				jButton3.setEnabled(false);
				jButton2.setEnabled(false);
			}
			else{
				jLabel2.setText("delete cancelled");
			}
		}
		
		
		if("anonymize".equals(e.getActionCommand())){
			Main_controller main_obj = new Main_controller();
			try {
				main_obj.mainFunction(getFolder());
				jLabel3.setText("Export Path: ");
				jLabel2.setText(getFolder()+"\\Export\\");
				if (Desktop.isDesktopSupported()) {
					File dir = new File(getFolder()+"\\Export\\");
					try {
						Desktop.getDesktop().open(dir);
						jButton2.setEnabled(false);
					} catch (IOException e1) {
						System.out.println(e1.getMessage());
					}
				}
			} catch (GeneralException e1) {
				jLabel3.setText("Error! ");
				jLabel2.setText(e1.getMessage());
				JOptionPane.showMessageDialog(getParent(), e1.getMessage());
				jButton2.setEnabled(false);
			}
		}
	}

	public Dimension getPreferredSize(){
		return new Dimension(300, 50);
	}

	public static void main(String args[])  {

		try { 
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		JFrame frame = new JFrame("BPELanon");
		UI_DirectoryChooser panel = new UI_DirectoryChooser(frame.getContentPane());
		frame.addWindowListener(
				new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				}
				);
		frame.getContentPane().add(panel);
		frame.setSize(panel.getPreferredSize());
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}


}
