/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class BPEL_Scanner {

	List<String> namespace = new LinkedList<String>();
	List<String> old_free = new LinkedList<String>();
	List<String> new_free = new LinkedList<String>();

	/*
	* edits the bpel file based on the associated WSDL files
	* @param file bpel file name
	* @param file_wsdl wsdl file name
	* @param oldAttributes list of old critical attributes
	* @param newArrtibutes list of the new critical attributes
	*/
	public void editBpel(String file,String file_wsdl,List<String> oldAttributes,List<String> newAttributes) {
		
		String targetnamespace =null;
		
		ReNamer renamer_object = new ReNamer();
		Utilities utils_obj = new Utilities();
		Registry reg_obj = new Registry();
		
		Document document = utils_obj.documentBuilder(file);
		Document document_wsdl = utils_obj.documentBuilder(file_wsdl);

		if(document_wsdl.hasChildNodes()){
			targetnamespace = utils_obj.getTargetNameSpace(document_wsdl.getChildNodes());
		}

		if(document.hasChildNodes()){
			namespace.clear();
			utils_obj.findNameSpacePrefix(document.getChildNodes(),targetnamespace,namespace);
		}

		if(document.hasChildNodes()){
			for (int i = 0; i < oldAttributes.size(); i++) {
				reg_obj.updateBpelNodeAttribute(document.getChildNodes(),oldAttributes.get(i),newAttributes.get(i),namespace);
				renamer_object.comitChanges(document, file);
			}	
		}


	}




	/*
	* Reads the FreeElementsGroup.xml file and makes a list of free elements 
	* @param file name of Bpel file
	* @param oldBpelvar list of old bpel variables
	* @param newBpelVar list of the new bpel variables
	*/


	public void freeElementGroup(String file,List<String> oldBpelvar,List<String> newBpelvar) throws DOMException, GeneralException {
		
		int j=0;
		String free_elements = null;
		String bpel_node_prefix="";
		
		Utilities utils_obj = new Utilities();
		ReNamer renam_obj = new ReNamer();
		Registry reg_obj = new Registry();
		
		Document document_bpel = utils_obj.documentBuilder(file);
		free_elements =this.getClass().getResource("resource/FreeElementGroup.xml").getPath();
		Document document = utils_obj.documentBuilder(free_elements);
		
		
		/*
		 * To check if the Bpel file nodes have any prefix 
		 */
		if(document_bpel.hasChildNodes()){	
			Node tempNode = document_bpel.getChildNodes().item(j++);
			while(tempNode!=null){
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
					if(tempNode.getNodeName().contains(":") && (tempNode.getNodeName().contains("process"))){
						bpel_node_prefix=tempNode.getNodeName().substring(0, (tempNode.getNodeName().length()-8))+":";
						break;
					}
				}
				tempNode = document_bpel.getChildNodes().item(j++);
			}
			freeElementsMatcher(document.getChildNodes(),document_bpel,bpel_node_prefix);
		}

		renam_obj.comitChanges(document_bpel, file);

		for (int i = 0; i < old_free.size(); i++) {
			if(document_bpel.hasChildNodes()){
				reg_obj.updateBpelFreeElements(document_bpel.getChildNodes(),old_free.get(i),new_free.get(i));
			}
		}

		//Logger log = new Logger();
		//log.createLog(file_log, old_free, new_free, file);
		renam_obj.comitChanges(document_bpel, file);

		if(document_bpel.hasChildNodes()){
			utils_obj.cdata(document_bpel.getChildNodes(),old_free,new_free);
			renam_obj.comitChanges(document_bpel, file);
		}
		oldBpelvar.addAll(old_free);
		newBpelvar.addAll(new_free);
		
		
	}

	/*
	* selects Free elements from the bpel file
	* @param nodelist the nodelist of the FreeElements XML
	* @param document_bpel document of the bpel file buit using a document builder
	* @param bpel_node_prefix node prefix used in the bpel file
	*/


	public void freeElementsMatcher(NodeList nodeList,Document document_bpel,String bpel_node_prefix) throws DOMException, GeneralException {
		Registry reg_obj= new Registry();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {

						Node currentAttribute = nodeMap.item(i1);
						if(document_bpel.hasChildNodes()){
							reg_obj.freeElementsFinder(document_bpel.getChildNodes(),bpel_node_prefix+tempNode.getNodeName(),currentAttribute.getNodeName());

							for (int j = 0; j < reg_obj.oldAttributes_bpel.size(); j++) {
								old_free.add(reg_obj.oldAttributes_bpel.get(j));
								new_free.add(reg_obj.newAttributes_bpel.get(j));
							}
						}
					}
				}
				if (tempNode.hasChildNodes()) {
					freeElementsMatcher(tempNode.getChildNodes(),document_bpel,bpel_node_prefix);
				}
			}
		}
	}

	/*
	* Edit the deployment descriptor
	* @param file deployment descriptor file name
	*/

	public void editdeploy(String file) {
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		Document document = utils_obj.documentBuilder(file);


		if(document.hasChildNodes()){
			editDeploymentDescriptor(document.getChildNodes());
			renamer_object.comitChanges(document, file);
		}
	}

	/*
	* Recursive function Edit the deployment descriptor 
	* @param nodelist nodelist of the deployment descripyor
	*/


	public void editDeploymentDescriptor(NodeList nodeList) {
		
		int j=0;
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							j=0;
							for (String oldAttribute : old_free) {
								String newAttribute = new_free.get(j++);
								if(currentAttribute.getNodeName().toLowerCase().compareTo("partnerlink")==0  && 
										(currentAttribute.getNodeValue().equals(oldAttribute))) { 
									currentAttribute.setNodeValue(newAttribute);
								}
							}

						}
					}

				if (tempNode.hasChildNodes()) {
					editDeploymentDescriptor(tempNode.getChildNodes());

				}
			}
		}
	}
	
	/*
	* anonymizes the targetNamespace
	*/
	
	public void anonTargetNS (List<String> file_wsdls, List<String> file_xsds, String file_bpel, String file_deploy) throws GeneralException {
		
		List<String> oldTargetNS = new LinkedList<String>();
		List<String> newTargetNS = new LinkedList<String>();
		
		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();
		
		//get the targetNamespaces from all wsdl files
		for(int i = 0; i < file_wsdls.size(); i++) {
			Document document = utils_obj.documentBuilder(file_wsdls.get(i));
			oldTargetNS.add(utils_obj.getTargetNameSpace(document.getChildNodes()));
			newTargetNS.add("http://" + renamer_object.anonymizer());
		}
		
		//edit namespaces in bpel
		
		
		//edit namespaces in wsdl
		for(int i = 0; i < file_wsdls.size(); i++) {
			Document document_wsdl = utils_obj.documentBuilder(file_wsdls.get(i));
			editTargetNS(document_wsdl.getChildNodes(), oldTargetNS, newTargetNS);
			renamer_object.comitChanges(document_wsdl, file_wsdls.get(i));
		}
		
		//edit namespaces in xsd
		for(int i = 0; i < file_xsds.size(); i++) {
			Document document_xsd = utils_obj.documentBuilder(file_xsds.get(i));
			editTargetNS(document_xsd.getChildNodes(), oldTargetNS, newTargetNS);
			renamer_object.comitChanges(document_xsd, file_xsds.get(i));
		}
		
		//edit namespaces in deploy
		Document document_deploy = utils_obj.documentBuilder(file_deploy);
		if(document_deploy.hasChildNodes()){
			editTargetNS(document_deploy.getChildNodes(), oldTargetNS, newTargetNS);
		}
		renamer_object.comitChanges(document_deploy, file_deploy);
		
		Document document_bpel = utils_obj.documentBuilder(file_bpel);
		if(document_bpel.hasChildNodes()){
			editTargetNS(document_bpel.getChildNodes(), oldTargetNS, newTargetNS);
			//editliteralNS(document_bpel, oldTargetNS, newTargetNS);
			
		}
		renamer_object.comitChanges(document_bpel, file_bpel);
		
	}
	
	/*
	* edits the targetNamespace
	*/
	public void editTargetNS(NodeList nodeList, List<String> oldTargetNS, List<String> newTargetNS ) {
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.hasAttributes()) {
				NamedNodeMap nodeMap = tempNode.getAttributes();
				for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
					Node currentAttribute = nodeMap.item(i1);
					for(int i2 = 0; i2 < oldTargetNS.size(); i2++) {
						if(currentAttribute.getNodeValue().equals(oldTargetNS.get(i2))){
							currentAttribute.setNodeValue(newTargetNS.get(i2));
							
						}
					}
				}
			}
			if (tempNode.hasChildNodes()) {
				editTargetNS(tempNode.getChildNodes(), oldTargetNS, newTargetNS );
			}
		}
		
	}
	/*
	* edits the targetNamespace in bpel:literal
	*/
	public void editliteralNS(Document document_bpel, List<String> oldTargetNS, List<String> newTargetNS ) {
		NodeList nodeList = document_bpel.getElementsByTagName("bpel:literal");
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.hasChildNodes()) {
				NodeList child_nodes = tempNode.getChildNodes();
				for (int i1 = 0; i1 < child_nodes.getLength(); i1++) {
					Node child_node = child_nodes.item(i1);
					System.out.println(child_node.getNamespaceURI());
					for(int i3 = 0; i3 < oldTargetNS.size(); i3++) {
						if(child_node.getNamespaceURI().equals(oldTargetNS.get(i3))){
							String tns = child_node.getNodeName().split(":")[0];
							String nodeName = child_node.getNodeName().split(":")[1];
							document_bpel.renameNode(child_node, newTargetNS.get(i3), nodeName);
							child_node.setPrefix(tns);
							break;
						}
					}
				}
			}
		}
	}
	
	
}
