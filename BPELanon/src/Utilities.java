/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Utilities {

	private String target_namespace ;

	/*
	 * getters and setters
	 */
	public String getTarget_namespace() {
		return target_namespace;
	}
	public void setTarget_namespace(String target_namespace) {
		this.target_namespace = target_namespace;
	}


	public File[] finder(String dirName,final String extension){
		File dir = new File(dirName);
		return dir.listFiles(new FilenameFilter() { 
			public boolean accept(File dir, String filename){ 
				return filename.endsWith("."+extension); 
			}
		} );
	}




	/*
	 * Document Builder for files
	 */

	public Document documentBuilder(String xmlfilename) {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		Document document = null;
		try {
			builder = builderFactory.newDocumentBuilder();
		}
		catch(ParserConfigurationException e){

			e.printStackTrace();
		}
		try{
			document = builder.parse(new FileInputStream(xmlfilename));
		}
		catch(SAXException e){
			e.printStackTrace();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		return document;
	}

	/*
	 * Creates backup of the files
	 */
	public void backupCreator(String file, String file_edited) {
		ReNamer renamer_object = new ReNamer();

		Document document= documentBuilder(file);
		renamer_object.comitChanges(document, file_edited);
	}

	/*
	 * Finds the name space of the wsdl file
	 */
	public String getTargetNameSpace(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.getNodeName().equals("wsdl:definitions") || tempNode.getNodeName().equals("definitions") && tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeName().equals("targetNamespace")){
							setTarget_namespace(currentAttribute.getNodeValue());
						}
					}
					if (tempNode.hasChildNodes()) {
						getTargetNameSpace(tempNode.getChildNodes());
					}				
				}
			}
		}
		return getTarget_namespace();
	}

	
	
	/*
	 * Used to find the namespace prefixes associated with the file  
	 */

	public void findNameSpacePrefix(NodeList nodeList,String targetnamespace,List<String> namespace) {

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {

					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeValue().equals(targetnamespace)){
							if(currentAttribute.getNodeName().startsWith("xmlns:") && 
									(tempNode.getNodeName().contains("process") || tempNode.getNodeName().contains("definitions")
											|| tempNode.getNodeName().contains("schema"))){
								namespace.add(currentAttribute.getNodeName().substring(6, currentAttribute.getNodeName().length())+":");
							}
						}
					}
				}

				if (tempNode.hasChildNodes()) {
					findNameSpacePrefix(tempNode.getChildNodes(),targetnamespace,namespace);
				}
			}
		}
	}

	/*
	 * match the name space prefix associated with the wsdl file 
	 */
	public void matchNameSpacePrefix(NodeList nodeList,ArrayList<String> namespace) {

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeValue().equals("http://schemas.xmlsoap.org/wsdl/")){
							if(currentAttribute.getNodeName().startsWith("xmlns:")){
								namespace.set(0,currentAttribute.getNodeName().substring(6, currentAttribute.getNodeName().length())+":");
							}
						}
						if(currentAttribute.getNodeValue().equals("http://docs.oasis-open.org/wsbpel/2.0/plnktype")){
							if(currentAttribute.getNodeName().startsWith("xmlns:")){
								namespace.set(1,currentAttribute.getNodeName().substring(6, currentAttribute.getNodeName().length())+":");
							}
						}

					}
				}

				if (tempNode.hasChildNodes()) {
					matchNameSpacePrefix(tempNode.getChildNodes(),namespace);

				}
			}
		}
	}

	/*
	 * gets target namespace of xsd file
	 */

	public String getTargetNameSpaceXSD(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.getNodeName().contains("schema") ) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
						Node currentAttribute = nodeMap.item(i1);
						if(currentAttribute.getNodeName().equals("targetNamespace")){
							setTarget_namespace(currentAttribute.getNodeValue());
						}
					}
					if (tempNode.hasChildNodes()) {
						getTargetNameSpace(tempNode.getChildNodes());
					}				
				}
			}
		}
		return getTarget_namespace();
	}

	/*
	 * updates the file names in the import statements
	 */
	public void editImports(NodeList nodeList) throws GeneralException {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if(tempNode.getNodeName().contains("import")){
					if (tempNode.hasAttributes()) {
						NamedNodeMap nodeMap = tempNode.getAttributes();
						for (int i1 = 0; i1 < nodeMap.getLength(); i1++) {
							Node currentAttribute = nodeMap.item(i1);
							if(currentAttribute.getNodeName().toLowerCase().contains("location")) { 
								String value = currentAttribute.getNodeValue();
								value = value.substring(0, value.indexOf("."))+"_edited"+
										value.substring(value.indexOf("."),value.length());
								currentAttribute.setNodeValue(value);
							}

						}
					}
				}
				if (tempNode.hasChildNodes()) {
					editImports(tempNode.getChildNodes());

				}
			}
		}
	}

	
	
	/*
	 * updates the anonymix=zed free varibles used in CDATA expressions 	
	 */

	public void cdata(NodeList nodeList,List<String> old_free, List<String> new_free) throws GeneralException {
		String temp = null;
		int l =0;
		int start = 0;
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.CDATA_SECTION_NODE) {
				String value = tempNode.getNodeValue();
				for (int j = 0; j < value.length(); j++) {
					temp=null;
					if(value.charAt(j)=='$'){
						j++;
						start = j;
						varfinder: while(Character.isLetterOrDigit(value.charAt(j))){
							temp=temp+value.charAt(j++);
							if(j == value.length()) {
								break varfinder;
							}
						}
						temp=temp.substring(4); 
						l=0;
						for (String li : old_free) {
							String new_varible = new_free.get(l++);
							if(temp.equals(li)){
								value= value.substring(0, start)+new_varible+value.substring(start+temp.length());
								tempNode.setNodeValue(value);
								j = start + new_varible.length();
								break;
							}
						}
					}

				}
			}
			if (tempNode.hasChildNodes()) {
				cdata(tempNode.getChildNodes(),old_free,new_free);

			}

		}
	}

	/*
	 * Remove comments in the project files
	 */
	public void removeCommenting(String file) {

		Utilities utils_obj = new Utilities();
		ReNamer renamer_object = new ReNamer();

		Document document = utils_obj.documentBuilder(file);		
		removeComments(document.getChildNodes());
		renamer_object.comitChanges(document, file);
	}

	/*
	 * recursive method to remove comments within the files
	 */
	private void removeComments(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.COMMENT_NODE) {
				tempNode.setNodeValue("");
			}

			if (tempNode.hasChildNodes()) {
				removeComments(tempNode.getChildNodes());
			}				
		}
	}

}



