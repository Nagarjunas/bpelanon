/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
public class GeneralException extends Exception {

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public GeneralException(String message){
	        super(message);
	    }

}
