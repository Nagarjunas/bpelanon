/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;


public class ReNamer {

	List<Integer> used_anonyms = new LinkedList<Integer>();
	
	/*
	 * Commit changes and write the document content to the file specified
	 * Accepts two inputs Document and a String
	 */

	public void comitChanges(Document document,String file) {

		try {
			TransformerFactory tFactory =  TransformerFactory.newInstance();
			Transformer transformer;
			transformer = tFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			if (document.getDoctype() != null) {
				String systemValue = (new File (document.getDoctype().getSystemId())).getName();
				transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,systemValue);
			}
			transformer.transform(domSource, new StreamResult(new FileOutputStream(file)));

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public Integer getLineLimit() throws GeneralException{
		Properties prop = new Properties();
		Integer line=null;
		try {
			prop.load(getClass().getClassLoader().getResourceAsStream("resource/config.properties"));
			line = Integer.parseInt(prop.getProperty("line_limit"));
			return line;
		} catch (Exception e) {
			throw new GeneralException("Cannot Access / Missing config.properties file");
		}


	}
	/*
	 * Returns a random string from the dictionary
	 */
	public String anonymizer() throws GeneralException {
		
		int i=0;
		int lineNumber = 0;
		String strLine= null;
		Random random_obj = new Random();
		
		lineNumber = random_obj.nextInt(getLineLimit()) ;
		
		while(checkForDuplicate(lineNumber)){
			lineNumber = random_obj.nextInt(getLineLimit()) ;
		}
		used_anonyms.add(lineNumber);
		
		try {
			FileInputStream fstream = new FileInputStream(this.getClass().getResource("resource/Dictionary.txt").getPath());
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			while ((strLine = br.readLine()) != null) {
				i++;
				if( lineNumber == i){
					break;
				}
			}
			br.close();
		} catch (IOException e) {
			throw new GeneralException(e.getMessage());
		}

		return strLine;

	}

	/*
	 * Checks for duplicate strings
	 */
	public boolean checkForDuplicate(Integer lineNumber) {
		for (Integer anon_list : used_anonyms) {
			if(anon_list==lineNumber){
				return true;
			}
		}
		return false;
	}
}
