/*
 * Copyright (C) Balu Venu Thayil
 * 				 email: st111189@stud.uni-stuttgart.de
 * 				 IAAS - University of Stuttgart 
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class Logger {

	FileWriter fw =null;
	BufferedWriter bw = null;
	
	public void createLog(File file_log, List<String> oldAttributes , List<String> newAttributes,String name){
		try {
			int i=0;
			if (!file_log.exists()) {
				file_log.createNewFile();	 
			}
			fw = new FileWriter(file_log.getAbsoluteFile(),true);
			bw = new BufferedWriter(fw);
			bw.write("\n****************"+name+"**************\n");
			for (String string : oldAttributes) {
				bw.write(string+" : "+newAttributes.get(i++)+"\n");
			}
			
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
